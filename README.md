## This is a curated list of design patterns in various languages. below you'll find links to other github projects. Note that this is NOT a place to learn algorithms - this list is for design patterns only. to see algorithms, check out another awesome list

### Index
- Patterns
- Books
- Websites
- General Design Patterns
- Other Awesome Lists

## Patterns

- Python
	* [by @faif](https://github.com/faif/python-patterns) This is a really well put together list of design patterns implemented in Python
- Objective-C
- Ruby
- Swift

## Books

## Websites

## General Design Patterns

## Other Awespome Lists

- awesome-algorithms
- awesome-big-data
- awesome-ruby
- awesome-swift
- awesome-iOS
